# Use case
A lot of times we want to build a block displaying content related to the
current page's content, sharing the same values for one or more fields. This
was usually done via building a custom block or creating a view but tampering it
in-code to provide it the values which were required. Using this module, you
won't have to do anything like that anymore!

# How it works
In most routes, the entity type is part of the route parameters. Take for
example `/node/{node}`, `/taxonomy/term/{taxonomy_term}`,
`/commerce_product/{commerce_product}` etc.

Taking this into account, along with the fact that when using as contextual
filter a entity field we also have access to that entity's field_name property
we were able to build a dynamic solution to retrieve the value(s) of that field
from the current route's entity. The code will identity properly whether the
field is using "value", "target_id" or something else as property.

# Configuration
Build your view and add the field you want as contextual filter.
In the `When the filter is NOT available` group choose the `Same field value
from route context` provided by this module.
In the new `Multiple values` select list that appears, choose how to handle
multiple values (OR, AND, Use first value).
In case of multiple values, you will have to also check the `Allow multiple
values` checkbox in the `More` group.
In case the contextual filter is optional (may have values, may have not) don't
forget to check the `Specify validation criteria` checkbox and in the `Action to
take if filter value does not validate` choose the `Display all results for the
specified field`.

# Advanced cases

### Override 

Though the name of the module implies that you should have the same entity type
and field name for the code to work, to cover scenarios where the entity type
and/or the field name are different from the current page's, you may use the
textfields in the `Override` group to set up any overrides.

### Use parent term

One of the latest additions to this module was the option to "Use parent as
argument". This is mostly useful for views requiring the usage of the "Has
taxonomy term ID (with depth)" plugin in order to filter content based on other
terms in the hierarchy based on the term(s) retrieved from the content. In that
context, the settings of your view should more-or-less match the following:
```yaml
plugin_id: taxonomy_index_tid_depth
default_action: default
exception:
  value: all
  title_enable: false
  title: All
title_enable: false
title: ''
default_argument_type: samefield_contextual_default
default_argument_options:
  multiple: or
  overridden_entity_type: node
  overridden_field_name: field_XXXX
  use_parent_term: 1
break_phrase: true
depth: 1
```
