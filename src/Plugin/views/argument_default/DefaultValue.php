<?php

namespace Drupal\views_samefield_contextual_default\Plugin\views\argument_default;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\views\Plugin\views\argument_default\ArgumentDefaultPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Default argument plugin to obtain the subjects of the current publication.
 *
 * @ViewsArgumentDefault(
 *   id = "samefield_contextual_default",
 *   title = @Translation("Same field value from route context")
 * )
 */
class DefaultValue extends ArgumentDefaultPluginBase implements CacheableDependencyInterface {

  public const MULTIPLE_VALUES_OR = 'or';
  public const MULTIPLE_VALUES_AND = 'and';
  public const MULTIPLE_VALUES_IGNORE = 'ignore';

  /**
   * The entity_type.manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * Constructs a new User instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entityTypeManager, RouteMatchInterface $route_match) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entityTypeManager;
    $this->routeMatch = $route_match;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('current_route_match')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options['multiple']['default'] = self::MULTIPLE_VALUES_OR;
    $options['overridden_entity_type']['default'] = '';
    $options['overridden_field_name']['default'] = '';
    $options['use_parent_term']['default'] = '';
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    $form['multiple'] = [
      '#type' => 'select',
      '#required' => TRUE,
      '#title' => $this->t('Multiple values'),
      '#description' => $this->t('Choose how to handle multiple values if found.'),
      '#options' => [
        self::MULTIPLE_VALUES_OR => $this->t('Or'),
        self::MULTIPLE_VALUES_AND => $this->t('And'),
        self::MULTIPLE_VALUES_IGNORE => $this->t('Use first value'),
      ],
      '#default_value' => $this->options['multiple'],
    ];
    $form['override'] = [
      '#type' => 'details',
      '#tree' => FALSE,
      '#title' => $this->t('Override'),
      '#description' => $this->t("In some cases, the entity type or the field name to lookup in the current route's context is different than the ones used in the view as contextual filter. If that's your case, specify below the machine name of the entity and/or field to lookup in the current route's context."),
    ];
    $form['override']['overridden_entity_type'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Entity type'),
      '#description' => $this->t('The machine name of the entity type (eg. route parameter) to lookup instead.'),
      '#default_value' => $this->options['overridden_entity_type'] ?? '',
    ];
    $form['override']['overridden_field_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Field name'),
      '#description' => $this->t('The machine name of the field to lookup instead.'),
      '#default_value' => $this->options['overridden_field_name'] ?? '',
    ];
    $form['taxonomy_term'] = [
      '#type' => 'details',
      '#tree' => FALSE,
      '#title' => $this->t('Taxonomy Term'),
      '#description' => $this->t('Advanced settings if the value you expect to receive from the URL targets a specific taxonomy term.'),
    ];
    $form['taxonomy_term']['use_parent_term'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use parent as argument'),
      '#description' => $this->t('If your entity matches a child but you want to limit the view based on the parent, check this checkbox.'),
      '#default_value' => $this->options['use_parent_term'] ?? FALSE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function submitOptionsForm(&$form, FormStateInterface $form_state, &$options = []) {
    $options['overridden_entity_type'] = $form_state->getValue('overridden_entity_type');
    $options['overridden_field_name'] = $form_state->getValue('overridden_field_name');
    $options['use_parent_term'] = $form_state->getValue('use_parent_term');
    $this->options = $options;
  }

  /**
   * {@inheritdoc}
   */
  public function getArgument() {
    $configuration = $this->argument->configuration;
    $entity_type = $configuration['entity_type'] ?? NULL;
    $field_name = $configuration['field_name'] ?? $this->options['overridden_field_name'];
    if (isset($entity_type, $field_name)) {
      $entity = $this->routeMatch->getParameter($entity_type);
      if ($entity && $entity->hasField($field_name) && !$entity->get($field_name)->isEmpty()) {
        $property = key($entity->get($field_name)->first()->getValue());
        $values = array_column($entity->get($field_name)->getValue(), $property);
        if (!empty($this->options['use_parent_term'])) {
          /** @var \Drupal\taxonomy\TermStorageInterface $termStorage */
          $termStorage = $this->entityTypeManager->getStorage('taxonomy_term');
          $parents = [];
          foreach ($values as $tid) {
            foreach ($termStorage->loadAllParents($tid) as $parent_term) {
              $parents[] = $parent_term->id();
            }
          }
          $values = $parents;
        }

        switch ($this->options['multiple']) {
          case self::MULTIPLE_VALUES_OR:
            return implode('+', $values);

          case self::MULTIPLE_VALUES_AND:
            return implode(',', $values);

          case self::MULTIPLE_VALUES_IGNORE:
          default:
            return reset($values);
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return Cache::PERMANENT;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    return ['url'];
  }

}
